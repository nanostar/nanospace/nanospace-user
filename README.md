# Welcome to  Nanospace project

Repository are currently into migration process to gitlab.
Old address is: 
[sourceforge](https://sourceforge.isae.fr/projects/nanospace)

# Running demo server

Visit the [Nanospace test server](https://dcas-nanostar.isae.fr) of ISAE-SUPAERO!

 - click on Subscribe
 - choose your username, role and password
 - log-in using chosen username and password
 - download an example, [TutorialNanostar.json](https://gitlab.isae-supaero.fr/nanostar/nanospace/nanospace-user/-/blob/master/examples/TutorialNanostar.json) on your local drive
 - click on project (on top right) > Import Project
 - choose the downloaded TutorialNanostar.json file 
 - you can "invite" other users by selecting "TutorialNanostar" in the Project Tree and therefore share your project with them (they need to be registered)



# Run your own demo server using docker

## Easiest way is by using Docker-compose deployment script

demo.bash script will : 

  - download docker images
  - load docker image
  - launch docker-compose
  - launch a browser

it suppose that you have docker and docker-compose installed.

```
bash demo.bash
```

to stop docker deployement: 

```
docker-compose down
```
## Docker build and run images on localhost (or how to do the same thing without docker-compose....)

```
git clone https://gitlab.isae-supaero.fr/nanostar/nanospace/nanospace-docker-images.git
cd nanospace-docker-images
```

### Neo4j database

```
docker run   -it  --publish=7474:7474 --publish=7687:7687     --volume=$HOME/neo4j/data:/data  --name neo4j-container  --rm --env NEO4J_AUTH=neo4j/secret   neo4j:3.2
```

### Backend - Java service (Spingboot)

Unfortunately, we haven't find yet an elegant way to connect the java-service to the neo4j, here is a not elegant way to solve the issue:
```
myIP=$(docker inspect neo4j-container | grep '"IPAddress"' | head -n 1 )
echo $myIP
IP=$(echo $myIP | sed s/\"IPAddress\":\ \"// | sed s/\",//)
echo $IP
docker run  -it  --publish=8888:8888 --volume=$HOME/neo4j/data:/data --name java-service-neo4j-container --rm --env NEO4J_URI=bolt://${IP}   java-service-neo4j:dev
```

### Frontend - Angular

```
docker run  -it  --publish=80:80 --name nanospace-angular-ui-container --rm  nanospace-angular-ui:dev 
```

And check on your favorite browser [localhost](http://localhost/)

## Deployment on a server


