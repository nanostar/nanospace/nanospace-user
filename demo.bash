#!/bin/bash


# -----------------------------------------------------------------------------
# Nanospace project: demo 
# -----------------------------------------------------------------------------
# This script launch a local demo of Nanospace.
#  - download docker images
#  - load docker image
#  - launch docker-compose
#  - launch a browser
# -----------------------------------------------------------------------------

#
echo "-- Downloading Docker images"
if [ -d nanospace-docker-images ]
then 
echo “directory exists”
else
git clone https://gitlab.isae-supaero.fr/nanostar/nanospace/nanospace-docker-images.git
# Check if the command succeeded
	if [ $? -ne 0 ]; then
	    echo "Cloning nanospace-docker-images repository failed !"
	    exit 1
	fi
fi
exit 1
echo "-- Deploying Docker images"
cd nanospace-docker-images
bash load-docker-images.bash
# Check if the command succeeded
if [ $? -ne 0 ]; then
    echo "The Docker images load failed !"
    exit 1
fi


echo "-- launching docker-compose (can be killed using 'docker-compose down' command)" 
docker-compose up -d
# Check if the command succeeded
if [ $? -ne 0 ]; then
    echo "'docker-compose up' failed !"
    exit 1
fi


echo "-- launching docker-compose"
firefox -new-tab -url http://localhost

echo "You may need to wait a bit and refresh your browser...."
echo "... after, for a first interaction: "
echo "click on 'Subscribe' link, and define a new user on the database"
echo "login with the created user"
echo "Feel free to import a project example - Top right Project > "
echo "Import project > select '$(pwd)/examples/Nimph.json'"








