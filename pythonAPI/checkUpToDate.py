#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 12:06:08 2021

@author: tgateau
"""

from optparse import OptionParser
import sys, traceback
import time

#defaultServer = 'http://dcas-devel-1/api/'
defaultServer = 'https://dcas-nanostar.isae.fr/api/'
defaultUser = 'user'
defaultPwd = 'pass'
defaultAltitudeID = 1
defaultAltitudeValue =1
parser = OptionParser()
parser.add_option("-i", "--altitudeID", dest="altitudeID",
                  help="which nanospace ID correspond to the altitude.",default=defaultAltitudeID)         
parser.add_option("-a", "--altitudeValue", dest="altitudeValue",
                  help="attitude wanted ",default=defaultAltitudeValue)                 
parser.add_option("-s", "--server", dest="server",
                  help="which NSS server to connect with ",default=defaultServer)
parser.add_option("-u", "--user", dest="user",
                  help="your username in Nanospace UI ",default=defaultUser)                  
parser.add_option("-p", "--pwd", dest="pwd",
                  help="your password in Nanospace UI ",default=defaultPwd)                     
                  
(options, args) = parser.parse_args()

from nanospace import Nanospace
nanospace = Nanospace(options.server,options.user, options.pwd)


class Var:
    def __init__(self,nanospace_id,nanospace_name):
        self.id = nanospace_id
        self.name = nanospace_name
        self.val = '?'
    def __repr__(self):
        return "<"+str(self.id)+":"+self.val+">"

    def getVal(self):
        try:
            #be carefull between string and formulas !! Check Python API...
            self.val = nanospace.get_formula_value(self.id)
            return self.val
            #print(self)
           
        except ValueError:
        		print ("Are you sure that you're using the right ID ("+str(self.idA)+")?")
        		print( "Exception in user code:")
        		print ('-'*60)
        		traceback.print_exc(file=sys.stdout)
        		print ('-'*60)          
    def setVal(self,newVal):
        nanospace.update_formula_value(self.id, self.name ,str(newVal))




idA = 2120
var_A = Var(idA,"A")
var_A.getVal()

 
idB = 2123
var_B = Var(idB,"B")  
var_B.getVal()    


dicoRelation = {var_A:var_B}


def s1(val):
    return val*val


def reCompute():
    #get list to run
    var_B.setVal(s1(float(var_A.val)))
    print ("done")
reCompute()  


oldVal = var_A.getVal()   
print("waiting")# , end="", flush=False)



while (True):
    #print(".      ", end="", flush=True)
    time.sleep(1)
    newVal = var_A.getVal()   
    if not (oldVal == newVal):
        print ("Value changed")
        print(var_A)
        reCompute()  
        print(var_B)
        oldVal = newVal


""" 
while (True):
    print(".      ", end="", flush=False)
    time.sleep(1)
"""
    
    
    
