#!/usr/bin/env python
# -*- coding: utf-8 -*-
from optparse import OptionParser
import sys, traceback

defaultAltitudeID = 1295
defaultAltitudeValue = 424.0370523579581
#defaultServer = 'http://dcas-devel-1/api/'
defaultServer = 'https://dcas-nanostar.isae.fr/api/'
defaultUser = 'test'
defaultPwd = 'test'

parser = OptionParser()
parser.add_option("-i", "--altitudeID", dest="altitudeID",
                  help="which nanospace ID correspond to the altitude.",default=defaultAltitudeID)         
parser.add_option("-a", "--altitudeValue", dest="altitudeValue",
                  help="attitude wanted ",default=defaultAltitudeValue)                 
parser.add_option("-s", "--server", dest="server",
                  help="which NSS server to connect with ",default=defaultServer)
parser.add_option("-u", "--user", dest="user",
                  help="your username in Nanospace UI ",default=defaultUser)                  
parser.add_option("-p", "--pwd", dest="pwd",
                  help="your password in Nanospace UI ",default=defaultPwd)                     
                  
(options, args) = parser.parse_args()

from nanospace import Nanospace
nanospace = Nanospace(options.server,options.user, options.pwd)

try:
    #be carefull between string and formulas !! Check Python API...
    print("Altitude value in Nanospace UI was (formula): "+str(nanospace.get_formula_value(options.altitudeID)))
    
except ValueError:
		print ("Are you sure that you're using the right ID ("+str(options.altitudeID)+")?")
		print( "Exception in user code:")
		print ('-'*60)
		traceback.print_exc(file=sys.stdout)
		print ('-'*60)  

nanospace.update_formula_value(options.altitudeID, 'altitude', str(options.altitudeValue)+"*[km]")

print("New Altitude value in Nanospace UI was (formula): "+str(nanospace.get_formula_value(options.altitudeID)))



#Nimph.orbit.raan <=> raanID












