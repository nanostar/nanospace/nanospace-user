#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 12:06:08 2021

@author: tgateau
"""

from optparse import OptionParser
import sys, traceback

defaultAltitudeID = 1295
defaultAltitudeValue = 424.0370523579581
#defaultServer = 'http://dcas-devel-1/api/'
defaultServer = 'https://dcas-nanostar.isae.fr/api/'
defaultUser = 'tg'
defaultPwd = 'tg'

parser = OptionParser()
parser.add_option("-i", "--altitudeID", dest="altitudeID",
                  help="which nanospace ID correspond to the altitude.",default=defaultAltitudeID)         
parser.add_option("-a", "--altitudeValue", dest="altitudeValue",
                  help="attitude wanted ",default=defaultAltitudeValue)                 
parser.add_option("-s", "--server", dest="server",
                  help="which NSS server to connect with ",default=defaultServer)
parser.add_option("-u", "--user", dest="user",
                  help="your username in Nanospace UI ",default=defaultUser)                  
parser.add_option("-p", "--pwd", dest="pwd",
                  help="your password in Nanospace UI ",default=defaultPwd)                     
                  
(options, args) = parser.parse_args()

from nanospace import Nanospace
nanospace = Nanospace(options.server,options.user, options.pwd)

marginDownId = 1734
try:
    #be carefull between string and formulas !! Check Python API...
    oldAltitude = nanospace.get_formula_value(options.altitudeID)
    #print("Altitude value in Nanospace UI was (formula): "+str(oldAltitude))
   
except ValueError:
		print ("Are you sure that you're using the right ID ("+str(options.altitudeID)+")?")
		print( "Exception in user code:")
		print ('-'*60)
		traceback.print_exc(file=sys.stdout)
		print ('-'*60)  

try:
    #be carefull between string and formulas !! Check Python API...
    marginDown = nanospace.get_formula_value(marginDownId)
    #print("marginDownId value in Nanospace UI was (formula): "+str(marginDown))
   
except ValueError:
		print ("Are you sure that you're using the right ID ("+str(marginDownId)+")?")
		print( "Exception in user code:")
		print ('-'*60)
		traceback.print_exc(file=sys.stdout)
		print ('-'*60)  


oldAltitude = oldAltitude.replace('*', '')
oldAltitude = oldAltitude.replace('[km]', '')

res=True
if (float(oldAltitude) >= 750):
    print("Warning: LOS requirements may be violated due to field     ID ("+str(options.altitudeID)+"): altitude <"+oldAltitude+">")
    res=False
    
if (float(marginDown)<10):  
    print("Warning: margins requirements may be violated due to field ID ("+str(marginDownId)+"): marginDown <"+marginDown+">")    
    res=False

if res:
    print("All seem OK!")



