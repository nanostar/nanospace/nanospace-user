import requests as rq
 

class Nanospace:   
    def login(self):
        """
        Connect to the database with login and password use in constructor

        You can change your password and username since they are properties of the class
        Return id token use to access to the nanospace API.
        Raise Value error if a problem occur
        :Exemple:

        nanospace.username = 'test'
        nanospace.password = 'test'
        nanospace.login()
        """
        r = rq.post(self.ApiBaseAddress + 'login',  json= {"username": self.username, "password": self.password })
        if r.status_code >= 200 and r.status_code < 300:
            return r.headers.get('Authorization')
        else:
            raise ValueError(str('error' + str(r.status_code)))
    
    def logout(self):
        """
        Erase token and header to communicate to the api.
        
        You cannot access to the api until you login again
        :Exemple:

        nanospace.logout()
        nanospace.get_value() # will raise an error because you are now undentified

        """
        self.token =None
        self.headers=None
        
    def get_string_value(self, id):
        """
            Return the value indicate as parameter.

            Raise ValueError if issues occure
            :Exemple:

            print(nanospace.get_value(89))
            1.53
        """
        r = rq.get(self.ApiBaseAddress + 'value/'+ str(id), headers=self.headers)
        r_data = r.json()
        if r.status_code >= 200 and r.status_code < 300:
            return r_data['value']
        else:
            raise ValueError(r_data)
    
    def update_string_value(self, id, name, value):
        """
           Update the value passed as id.
           Raise ValueError if errors occures

           :Exemple:
           nanospace.update_value(89, 'altitude', 'km', 8000)
        """
        r=rq.put(self.ApiBaseAddress + 'value/'+ str(id), json = { 'name':name, 'value': value }, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
           raise ValueError(r_data)
        else:
            return r_data['value']

    def get_formula_value(self, id):
        """
            Return the value indicate as parameter.

            Raise ValueError if issues occure
            :Exemple:

            print(nanospace.get_value(92))
            Get(78) + 5
        """
        r = rq.get(self.ApiBaseAddress + 'formula/' + str(id), headers=self.headers)
        r_data = r.json()
        if r.status_code >= 200 and r.status_code < 300:
            return r_data['formula']
        else:
            raise ValueError(r_data)

    def update_formula_value(self, id, name, value):
        """
           Update the formula passed as id.
           Raise ValueError if errors occures

           :Exemple:
           nanospace.update_value(92, 'multiplication', '5*3')
        """
        r = rq.put(self.ApiBaseAddress + 'formula/' + str(id), json={'name': name, 'formula': value}, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
            raise ValueError(r_data)
        else:
            #return r_data['formula']
            return r_data


    def _create_component(self, idParent, componentName):
        """
           Create a component children of the parent passed as id
           Return a JSON component

           :Exemple:
           nanospace._create_component(89, 'mySuperComponent')
        """
        r = rq.post(self.ApiBaseAddress + 'component?parentId=' + str(idParent), json={'name': componentName}, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
            raise ValueError(r_data)
        else:
            return r_data

    def _create_mode(self, idParent, modeName):
        """
        Create a mode of a component parent passed as id
        Return the a JSON component
 
        :Exemple:
        nanospace._create_mode(28, 'test-mode')
        """
 
        r = rq.post(self.ApiBaseAddress + 'mode?parentId=' + str(idParent), json={'name': modeName}, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
            raise ValueError(r_data)
        else:
            return r_data

    def _create_string_value(self, idParent, valueName, value):
        """
        Create a string of a mode parent passed as id
        Return the a JSON component

        :Exemple:
        nanospace._create_string_value(28, 'string_value', 'Hello-World')
        """
        r = rq.post(self.ApiBaseAddress + 'string?parentId=' + str(idParent), json={'name': valueName, 'value': value, 'type': 'string_value'}, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
            raise ValueError(r_data)
        else:
            return r_data

    def _create_formula_value(self, idParent, valueName, value):
        """
        Create a formula of a mode parent passed as id
        Return the a JSON component

        :Exemple:
        nanospace._create_formula_value(28, 'value', '5*8')
        """
        r = rq.post(self.ApiBaseAddress + 'formula?parentId=' + str(idParent), json={'name': valueName, 'formula': value, 'type': 'string_value'}, headers=self.headers)
        r_data = r.json()
        if r.status_code < 200 or r.status_code >= 300:
            raise ValueError(r_data)
        else:
            return r_data

    def _delete_value(self, idValue):
        """
        Delete a value which the id has been passed as parameter
        Return the a JSON component

        :Exemple:
        nanospace._delete_value(28)
        """
        r = rq.delete(self.ApiBaseAddress + 'value/' + str(idValue), headers=self.headers)
        # r_data = r.json()
        # if r.status_code < 200 or r.status_code >= 300:
        #     raise ValueError(r_data)
        # else:
        #     return r_data

    def _delete_mode(self, idMode):
        """
        Delete a mode which the id has been passed as parameter
        Return the a JSON component

        :Exemple:
        nanospace._delete_mode(28)
        """
        r = rq.delete(self.ApiBaseAddress + 'mode/' + str(idMode), headers=self.headers)
        # r_data = r.json()
        # if r.status_code < 200 or r.status_code >= 300:
        #     raise ValueError(r_data)
        # else:
        #     return r_data

    def _delete_component(self, idComponent):
        """
        Delete a component which the id has been passed as parameter
        Return the a JSON component

        :Exemple:
        nanospace._delete_component(28)
        """
        r = rq.delete(self.ApiBaseAddress + 'component/' + str(idComponent), headers=self.headers)
        # r_data = r.json()
        # if r.status_code < 200 or r.status_code >= 300:
        #     raise ValueError(r_data)
        # else:
        #     return r_data


    def setApiBaseAddress(self, url):
        '''
        Change the api base address into the url input paramter
        '''
        self.ApiBaseAddress = url


    def __init__(self, apiAdrress, username, password):
        '''
        Create an instance of Nanospace class with input username, password. Will try to log you with your input
        
        Can raise an error when trying to log in
        :Exemple:
        
        nano = Nanospace('http://localhost:8080/','test','test')
        '''
        self.username = username
        self.password = password
        self.ApiBaseAddress = apiAdrress
        self.token = self.login()
        self.headers = {'Authorization': self.token}

        
